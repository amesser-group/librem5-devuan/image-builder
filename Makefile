# SPDX-License-Identifier: GPL-3.0+

BOARD=imx6
TYPE=unstable
IMAGE=$(BOARD).img
COMPRESSED=$(IMAGE).xz
CHECKSUM=sha256sums
DIST=buster+ci
DEPS=\
  build-image   \
  root.sh       \
  conf/board    \
  conf/packages \
  $(NULL)


.PHONY: all
all: $(COMPRESSED)

$(IMAGE): tmp/$(IMAGE).stamp
tmp/$(IMAGE).stamp: local.sh-link $(DEPS)
	./build-image -b $(BOARD) -T $(TYPE) -d $(DIST)
	touch tmp/$(IMAGE).stamp

%.img.xz: %.img tmp/%.img.stamp
	xz -T0 --stdout $< > $@.tmp
	mv $@.tmp $@

$(CHECKSUM):
	sha256sum $(COMPRESSED) > $@.tmp
	mv $@.tmp $@

clean:
	rm -rf files/ tmp/ $(IMAGE) $(COMPRESSED) $(CHECKSUM)*

check:
	shellcheck -x ./build-image
	bash -n root.sh

publish: $(CHECKSUM)
	bin/sign-checksum
	bin/publish -T ${TYPE}

local.sh-link:
	if test -e local.sh; then \
		mkdir -p files; \
		ln -sf ../local.sh files; \
	fi

.PHONY: all publish local.sh-link
