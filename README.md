Libre M5 & related Image Builder
================================

This project started as a fork of https://source.puri.sm/Librem5/image-builder
with background of making an Devuan based image for the Libre M5
mobile phone. 

The original project is using vmdebbootstrap and shell script 
to generate the image. Since vmdeboostrap has bugs and
is considered legacy in favour of vmdb2, first step was 
to convert the build process to a vmdb2 and partly ansible
based workflow. Now in a second step, the need for vmdb2 has completely 
removed and the image is built with ansible only.

# Description of new style build scripts

## Quick-Start 

WARNING: This builder is in a very early state, it might fail unexpectedly.
Don't hesitate to contact me

To start make sure the following packages are available and installed on your
system:

* *ansible 2.7* (Take from testing or unstable)
* *gcc-aarch64-linux-gnu*
* *qemu-user-static*
* *uuu*: [NXP Flasher tool][7] (There is no packaging currently yet)

To download and build latest kernel from PuriSM and download pre-build bootloader
run:

    ansible-playbook -i inventory/build-librem5-devkit-software.ini build-software.yml

Noe be patient, this takes some time. The command will download the kernel 
source, pre-built u-boot loader from purism and compile the kernel packages 
from source. Afterwards the image can be build - this will take even longer. 
This currently needs root permissions:

    sudo ansible-playbook -i inventory/build-librem5-devkit-devuan-img.ini \
                          -e output_dir=/tmp/librem5 \
                          build-image.yml

The image and a script for flashing is found in output folder /tmp/librem5 afterwards

## Details


The new style build currently supports the following boards:

* *librem5-devkit*: The [Librem 5 Devkit][0]

and the following operating systems:
* *devuan*: [Devian Operating system][5]
 
The image is created using the ansible devops tool. It allows that same scripts 
used to configure the image, can also used to directly configure a running 
target system (e.g. remote through ssh or locally) This way the scripts can either
configure/setup the whole image or used later to configure/setup an already existing
and running target system.

<!-- * *pureos*: [PureOS][6] (untested since last change)
  -->

## Changes compared to original Puri SMm image for Libre M5 (Devkit)

* The buildscripts will build a Devuan based image without 
  any graphical packages
* The original PuriSM kernel configuration is very limited. 
  This build scripts will build an more comprehensive kernel 
  configuration including various network filesystems and
  filesystem drivers
* The root file system will use f2fs instead of ext4 since f2fs 
  suits better for MMC flash memory
* The root filesystem is about 7GiB instead of 3.5GiB

## Known issues

* The librem5-devkits display is currently not working. There is ongoing work 
  on fine tuning the display driver. The default image will support only
  HDMI connected display. To use the on-board display, Copy */usr/lib/linux-image-*/freescale/*lcdonly.dtb to /boot/dtbs/librem5-evk.dtb
  and reboot
* There is a problem with ethernet link going down. This is related to an MDIO 
  communication issue. It helps to do:

      ifconfig eth0 up
      mii-tool -r eth0


# Description of original Purism build scripts

This is the image builder for the Librem5 and related boards:

* *devkit*: The [Librem 5 Devkit][0]
* *ec-som*: Emcraft's [i.MX 8M SOM Starter Kit][1]
* *imx8*: [NXPs IMX8M Evaluation Kit][2]
* *imx6*: The Bounday Devices [Nit6QP_MAX][3]
* *qemu-x86_64*: The [QEMU Image][4]

It supports various distributions (PureOS *laboratory* and *purple*, Debian
*buster*).  Additionally it can add our on commit built CI packages to get
the latest versions of the Librem 5 related sofware.

Furthermore it can incorporate different kernel versions:

* *latest*: Fetch the most recently built kernel
* *ci*: deprecated, don't use
* *unstable*: Pre release kernels (that got some testing)
* *stable*: Released kernels (none of them exist yet)

So to build an image with all the latest software you'd use:

    ./build-image -T latest -b devkit -d buster+ci

## Build u-boot

The build_uboot.sh script can build u-boots for a number of targets

* *devkit-recovery*: This is to boot the [Librem 5 Devkit][0] via SDP
* *devkit*: The [Librem 5 Devkit][0]
* *ec-som*: Emcraft's [i.MX 8M SOM Starter Kit][1]
* *imx8*: [NXPs IMX8M Evaluation Kit][2]
* *imx6*: The Bounday Devices [Nit6QP_MAX][3]

From the build directory invoke build_uboot.sh

    cd build 
    ./build_uboot.sh -b devkit

## Creating a bootable SD card

This is only relevant for SD card booting on e.g. the Nitrogen or NXP
boards:

The SD card must be larger than 3G. Replace <x> with the SD card device.

    sudo dd if=imx6.img of=/dev/sd<x> bs=4M

or a bit more verbose and using the compressed image

    bin/write-image of=/dev/sd<x>

Note: All data on the SD card will be overwritten.

## Running the tests

You can run tests against an installed board using

    sudo apt -y install ruby-serverspec
    cd tests
    TARGET_HOST=<your_board_ip> rake spec:nitrogen6

# Authors

This project started as a fork of https://source.puri.sm/Librem5/image-builder. Most of
these work's files are still available unchanged in this project for reference. However, 
they are not used when building with ansible. Copyright of all these files is at Purism https://puri.sm.

The ansible and files have been created by Andreas Messer <andi@bastelmap.de>.


[0]: https://developer.puri.sm/Librem5/Hardware_Reference.html#librem-5-devkit
[1]: https://www.emcraft.com/products/868
[2]: https://www.nxp.com/support/developer-resources/run-time-software/i.mx-developer-resources/evaluation-kit-for-the-i.mx-8m-applications-processor:MCIMX8M-EVK
[3]: https://boundarydevices.com/product/nitrogen6max/
[4]: https://developer.puri.sm/Librem5/Development_Environment/Boards/qemu.html#qemu
[5]: https://devuan.org
[6]: https://puri.sm/posts/tag/pureos/
[7]: https://github.com/NXPmicro/mfgtools