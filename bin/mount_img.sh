#!/bin/bash
#
# Copyright (C) 2017 Purism SPC
#
# SPDX-License-Identifier: GPL-3.0+
#

set -e

if [ "x$1" == "x" ]; then
  echo "Please specify the image to mount" 1>&2
  exit 1
fi

bootimg=$1

mkdir root
sudo losetup -P /dev/loop0 $bootimg
sudo mount /dev/loop0p1 root
