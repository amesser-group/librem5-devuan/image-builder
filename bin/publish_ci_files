#!/bin/bash
#
# Copyright (C) 2017-2018 Purism SPC
#
# SPDX-License-Identifier: GPL-3.0+
#

set -e

DATE=$(date --iso)
REMOTE=librem5@storage.puri.sm
BUILD_DIR=""
OUT_DIR=""
BUILD_TYPE="ci"

usage() {
  echo "Usage : $1 "
  echo "    -h Display this help message."
  echo "    -b build number"
  echo "    -t a tag for the type of build"
  echo "    -o output path"
  echo "    -T build type [ci|unstable]"
}

while getopts ":hb:t:o:T:" opt; do
  case ${opt} in
    b)
      BUILD_NUMBER=$OPTARG
      echo "Build number : ${BUILD_NUMBER}"
      ;;
    t)
      TAG=$OPTARG
      echo "Tag : ${TAG}"
      ;;
    o)
      OUT_DIR=$OPTARG
      echo "Out dir : ${OUT_DIR}"
      ;;
    T)
      BUILD_TYPE=$OPTARG
      echo "Build type : ${BUILD_TYPE}"
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      usage "$0"
      exit 0
      ;;
    h )
      usage "$0"
      exit 0
      ;;
  esac
done

shift "$((OPTIND-1))"
FILES="$@"

echo "Copying files ${FILES}"

[ -z ${TAG} ] || BUILD_DIR="${TAG}-"
BUILD_DIR+="${DATE}"
[ -z ${BUILD_NUMBER} ] || BUILD_DIR+="-B${BUILD_NUMBER}"
TARGET="librem5/binaries/${BUILD_TYPE}/${BUILD_DIR}"
[ -z ${OUT_DIR} ] || TARGET+="/${OUT_DIR}"

echo "Uploading to ${REMOTE}:${TARGET}"

ssh -p 3376 ${REMOTE}  "mkdir -p ${TARGET}"
ssh -p 3376 ${REMOTE}  "mkdir -p librem5/binaries/${BUILD_TYPE}/latest/${OUT_DIR}"

for FILE in ${FILES}; do 
    echo "Uploding file ${FILE}"
    BASE=`basename ${FILE}`
    scp -P 3376 -r ${FILE} "${REMOTE}:${TARGET}"
    ssh -p 3376 ${REMOTE} "ln -rnsf ${TARGET}/${BASE} librem5/binaries/${BUILD_TYPE}/latest/${OUT_DIR}/${BASE}"
done

