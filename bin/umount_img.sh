#!/bin/bash
#
# Copyright (C) 2017 Purism SPC
#
# SPDX-License-Identifier: GPL-3.0+
#

sudo umount /dev/loop0p1
sudo losetup -d /dev/loop0
