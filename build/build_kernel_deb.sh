#!/bin/sh
#
# Copyright (C) 2017 Purism SPC
#
# SPDX-License-Identifier: GPL-3.0+
#

# Passed in via the environment
MAKE_OPTS="${MAKE_OPTS}"

set -u
set -e

CROSS_COMPILER=aarch64-linux-gnu-
ARCH=arm64
COMPILER_ARCH="${ARCH}"
KERNEL_OUTPUT=../../files
CWD=`pwd`
pureos_board=imx6
DTS_FILE=
RSI_VER=RS9116.NB0.NL.LNX.PURISM_FW_UPGRADE.1.0.5


build_redpine () {
    tar -xf files/${RSI_VER}.tgz

    RSI_BUILD_PATH=${RSI_VER}/rsi/
    ESCAPED_KERNEL_DIR=$(echo "${LINUX_DIR}" | sed 's/\//\\\//g')

    sed "s/KERNELDIR=.*/KERNELDIR=${ESCAPED_KERNEL_DIR}/" -i ${RSI_BUILD_PATH}/Makefile

    cd ${RSI_BUILD_PATH}
    make ${MAKE_OPTS} ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILER} -C ${CWD}/${LINUX_DIR} M=$PWD modules
    cd ${CWD}/${RSI_BUILD_PATH} && tar -cf ${CWD}/../files/rsi_upgrade.tar *.ko
    cd ${CWD}/${RSI_VER}/Firmware && tar -cf ${CWD}/../files/rsi_firmware.tar *
    cd ${CWD}
}


usage() {
  echo "Usage : $1 "
  echo "    -h Display this help message."
  echo "    -b [imx6|imx8|ec-som|devkit] select the board type."
}

while getopts ":hb:" opt; do
  case ${opt} in
    b)
      pureos_board=$OPTARG
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      usage $0
      exit 0
      ;;
    h )
      usage $0
      exit 0
      ;;
  esac
done

LINUX_DIR="linux-${pureos_board}"

case ${pureos_board} in 
  imx6)
    KERNEL_BRANCH=purism-nitrogen6_4.16.x
    KERNEL_REPO=https://source.puri.sm/Librem5/linux-nitrogen6.git
    DOT_CONFIG=imx6_dot_config
    CROSS_COMPILER=arm-linux-gnueabihf-
    ARCH=arm
    COMPILER_ARCH=armhf
    GIT_OPTS="--depth 1"
    ;;
  imx8)
    KERNEL_BRANCH=imx8-4.21-evk
    KERNEL_REPO=https://source.puri.sm/Librem5/linux-next.git
    DOT_CONFIG=imx8mq-evk_defconfig
    GIT_OPTS="--depth 1"
    ;;
  ec-som)
    KERNEL_BRANCH=imx8-4.18-wip
    KERNEL_REPO=https://source.puri.sm/Librem5/linux-emcraft.git
    DOT_CONFIG=emcraft-som-imx8_defconfig
    GIT_OPTS="--depth 1"
    ;;
  devkit)
    KERNEL_BRANCH=imx8-4.18-wip
    KERNEL_REPO=https://source.puri.sm/Librem5/linux-emcraft.git
    DOT_CONFIG=librem5-evk_defconfig
    GIT_OPTS="--depth 1"
    ;;
  \?)
    echo "unrecognized board type ${pureos_board}"
    exit 1
    ;;
esac

if [ ! -e ${LINUX_DIR} ]; then
  echo "Cloning kernel"
  git clone ${GIT_OPTS} -b ${KERNEL_BRANCH} ${KERNEL_REPO} ${LINUX_DIR}
  cd ${LINUX_DIR}
else
  cd ${LINUX_DIR}
  echo "Updating kernel"
  git pull ${GIT_OPTS} ${KERNEL_REPO} ${KERNEL_BRANCH}
  git checkout ${KERNEL_BRANCH}
fi

if [ ! -e .config ]; then
  echo "Configuring kernel"
  if [ -e arch/${ARCH}/configs/${DOT_CONFIG} ]; then
    cp arch/${ARCH}/configs/${DOT_CONFIG} .config
  else
    cp ../${DOT_CONFIG} .config
  fi
fi

if [ -n "${DTS_FILE}" ]; then
  echo "Adding device tree file"
  cp ../${DTS_FILE} arch/${ARCH}/boot/dts/
fi

echo "Building kernel"
make ${MAKE_OPTS} ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILER} bindeb-pkg

echo "Copying kernel"
mkdir -p ${KERNEL_OUTPUT}
cp ../*${COMPILER_ARCH}.deb ${KERNEL_OUTPUT}

cd ${CWD}

case "${pureos_board}" in
    devkit)
        build_redpine
    ;;
    *)
    ;;
esac

echo ${KERNEL_REPO} > kernel-git-${ARCH}.txt
git log | head -n 1 | awk '{ print $2 };' >> kernel-git-${ARCH}.txt

cp kernel-git-${ARCH}.txt ../files/kernel-git-${ARCH}.txt
