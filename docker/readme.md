## Build the docker image

docker build -t debian_build .

## Run the docker image

docker run -ti -v /dev:/dev -v <path to debian_qemu_scripts>:/src --privileged debian_build

## Build the image in the container

cd /src
./build-image

