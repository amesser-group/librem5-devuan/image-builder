require 'spec_helper'

describe command('hostname') do
  its(:stdout) { should match /^pureos$/ }
end

describe service('dbus') do
  it { should be_running.under('systemd') }
end

