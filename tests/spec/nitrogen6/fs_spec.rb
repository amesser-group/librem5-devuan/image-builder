require 'spec_helper'

describe '/boot' do
  it 'is on a separate partition' do
    expect(file '/boot').to be_mounted
  end
end
