require 'spec_helper'

# the QEMU image currently has no graphics adapter so skip tests
# there for now
describe service('weston'), :unless => is_qemu do
  it { should be_running.under('systemd') }
end
