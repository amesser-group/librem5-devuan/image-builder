
require 'spec_helper'

describe "Detect modem and its ports", :unless => is_qemu do
  describe command('mmcli -m 0') do
    its(:stdout) { should match /manufacturer: 'Sierra Wireless, Incorporated'$/ }
    its(:stdout) { should match /model: 'MC7455'$/ }
    its(:stdout) { should match /ports: 'ttyUSB0 \(qcdm\), ttyUSB2 \(at\), cdc-wdm0 \(qmi\), cdc-wdm1 \(qmi\), wwan0 \(net\), wwan1 \(net\)'$/ }
  end
end

