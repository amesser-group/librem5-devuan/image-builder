require 'spec_helper'

describe package('systemd-sysv') do
  it { should be_installed }
end

describe service('systemd-timesyncd') do
  it { should be_running.under('systemd') }
end

# Logging to the jounral
describe file('/var/log/journal') do
  it { should be_a_directory }
end

describe service('systemd-journald') do
  it { should be_running.under('systemd') }
end

# Make sure there are no systemd units in the failed state
describe command('systemctl --failed') do
  its(:stdout) { should contain '0 loaded' }
end
