require 'spec_helper'

describe "uboot boot scripts" do
  describe file('/boot/boot.scr') do
    it { should be_file }
  end

  describe file('/boot/boot.txt') do
    it { should be_file }
  end

  describe file('/boot/6x_bootscript') do
    it { should be_symlink }
  end
end

describe "uboot boot upgrade" do
  describe file('/boot/upgrade.scr') do
    it { should be_file }
  end

  describe file('/boot/u-boot.nitrogen6qp_max') do
    it { should be_file }
  end

  describe file('/boot/6x_upgrade') do
    it { should be_file }
  end
end
