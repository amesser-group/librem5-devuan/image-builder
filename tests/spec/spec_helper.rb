require 'serverspec'
require 'net/ssh'

set :backend, :ssh
set :sudo_password, '123456'

host = ENV['TARGET_HOST']
port = ENV['TARGET_PORT'] || 22

options = Net::SSH::Config.for(host)

options[:user] ||= 'purism'
options[:password] ||= '123456'
options[:port] ||= port
# All new test images have a new host key
# and the account details are well known
options[:verify_host_key] ||= false

set :host,        options[:host_name] || host
set :ssh_options, options

def is_qemu()
  virt = host_inventory['virtualization']
  virt ? virt[:system] == 'qemu' : false
end
